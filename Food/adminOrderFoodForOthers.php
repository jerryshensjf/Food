<?php
include "include/isadmin.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>User Info.</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
    <?php
            include("foodmanager.php");
            $id=0;
            if(isset($_REQUEST["id"])){
            $id = $_REQUEST["id"];
        }
    ?>
    
    <body>
        <div id="wrapper">
        <?php include 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <!-- Fetch Rows -->
                        <table class="aatable">
                            <tr>
                                <th>ID</th>
                                <th>EmpID</th>
                                <th>User Name</th>
                                <th>Full Name</th>
                                <th>Sex</th>
                                <th>IsAdmin</th>
                                <th>Operation</th>
                            </tr>
                            <?php
                            $userData = getUserInfo($id);
                            
                            for($index=0;$index < count($userData);$index++){
                                $user = $userData[$index];
                                echo "<tr>";
                                echo "<td>".$user->get_id()."</td>";
                                echo "<td>".$user->get_empid()."</td>";                            
                                echo "<td>".$user->get_username()."</td>";
                                echo "<td>".$user->get_firstname()." ".$user->get_lastname()."</td>";
                                echo "<td>".$user->get_sex()."</td>";
                                echo "<td>".$user->get_isadmin()."</td>";
                                echo  "<td><form action='adminOrderFoodForOthers2.php' method='post'><input type='hidden' name='user_id' value='".$user->get_id()."'/><input type='submit' value='Order'/></form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </table>
                    </div>
                    <!-- end div#welcome -->			
                    
                </div>
                <!-- end div#content -->
                <div id="sidebar">
                    <!--ul-->
                        <?php if ($_SESSION['isAdmin'] ){
                                include 'include/adminnav.php';
                                }else{
                                  include 'include/usernav.php';
                                } ?>
                        <!-- end navigation -->
                            <?php include 'include/updates.php'; ?>
                        <!-- end updates -->
                    <!--/ul-->
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
