<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include "foodmanager.php";

$user = new User();
$user->set_empid($_POST['empid']);
$user->set_username($_POST['username']);
$user->set_firstname($_POST['firstname']);
$user->set_lastname($_POST['lastname']);
$user->set_sex($_POST['sex']);
$user->set_password($_POST['password']);
$confirmpassword=$_POST['confirmpassword'];

if(registerUser($user,$confirmpassword)){
    if (login($user)) {
        Header("Location:userhomepage.php");
    }else {
       Header("Location:index.php");
    }
}else {
     Header("Location:register.php");
}

?>
