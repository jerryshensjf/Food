<?php
/*
DB Configuration information is stored here.
Ideally you should read these values from an 
external properties file.
*/

class FoodConf{
    private $databaseURL = "localhost";
    private $databaseUName = "root";
    private $databasePWord = "";
    private $databaseName = "food";

    function get_databaseURL(){
        return $this->databaseURL;
    }
    function get_databaseUName(){
        return $this->databaseUName;
    }
    function get_databasePWord(){
        return $this->databasePWord;
    } 
    function get_databaseName(){
        return $this->databaseName;
    } 
}
?>