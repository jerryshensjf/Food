-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.32 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.1.0.4545
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 food 的数据库结构
drop database if exists `food` ;
create database `food` /*!40100 COLLATE 'utf8_general_ci' */;
use `food` ;

-- 导出  表 food.mealorderitems 结构
CREATE TABLE IF NOT EXISTS `mealorderitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mealorder_id` int(11) NOT NULL,
  `menuitem_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

-- 正在导出表  food.mealorderitems 的数据：41 rows
DELETE FROM `mealorderitems`;
/*!40000 ALTER TABLE `mealorderitems` DISABLE KEYS */;
INSERT INTO `mealorderitems` (`id`, `mealorder_id`, `menuitem_id`, `price`, `amount`) VALUES
	(1, 3, 1, 10.00, 1),
	(2, 4, 1, 10.00, 1),
	(3, 5, 4, 10.00, 1),
	(6, 8, 1, 10.00, 1),
	(7, 9, 4, 10.00, 1),
	(8, 10, 1, 10.00, 1),
	(9, 11, 4, 10.00, 1),
	(10, 12, 1, 10.00, 1),
	(11, 13, 1, 10.00, 1),
	(12, 14, 4, 10.00, 1),
	(13, 15, 4, 10.00, 1),
	(14, 16, 1, 10.00, 1),
	(15, 17, 4, 10.00, 1),
	(16, 18, 1, 10.00, 1),
	(17, 19, 4, 10.00, 1),
	(29, 31, 4, 10.00, 1),
	(30, 32, 1, 10.00, 1),
	(31, 33, 4, 10.00, 3),
	(32, 34, 4, 10.00, 4),
	(33, 35, 4, 10.00, 4),
	(37, 39, 1, 10.00, 2),
	(35, 37, 6, 10.00, 2),
	(38, 40, 4, 10.00, 2),
	(39, 41, 4, 10.00, 2),
	(40, 0, 6, 10.00, 2),
	(41, 42, 4, 10.00, 2),
	(42, 43, 4, 10.00, 2),
	(43, 44, 7, 10.00, 2),
	(44, 45, 7, 10.00, 2),
	(45, 46, 6, 10.00, 2),
	(46, 47, 6, 10.00, 2),
	(47, 48, 6, 10.00, 2),
	(48, 49, 6, 10.00, 2),
	(49, 50, 6, 10.00, 2),
	(52, 53, 13, 10.00, 3),
	(51, 52, 6, 10.00, 2),
	(53, 54, 1, 10.00, 1),
	(54, 55, 1, 10.00, 2),
	(55, 56, 38, 12.00, 1),
	(57, 58, 37, 10.00, 1),
	(58, 59, 37, 10.00, 1),
	(60, 61, 37, 10.00, 1),
	(61, 62, 37, 10.00, 1),
	(63, 64, 37, 10.00, 1),
	(64, 65, 37, 10.00, 1),
	(65, 66, 37, 10.00, 1),
	(66, 67, 37, 10.00, 1),
	(67, 68, 37, 10.00, 1),
	(68, 69, 38, 12.00, 1);
/*!40000 ALTER TABLE `mealorderitems` ENABLE KEYS */;


-- 导出  表 food.mealorders 结构
CREATE TABLE IF NOT EXISTS `mealorders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(20) DEFAULT NULL,
  `promotion` varchar(2000) DEFAULT NULL,
  `isActive` char(1) DEFAULT 'Y',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

-- 正在导出表  food.mealorders 的数据：3 rows
DELETE FROM `mealorders`;
/*!40000 ALTER TABLE `mealorders` DISABLE KEYS */;
INSERT INTO `mealorders` (`id`, `user_id`, `order_time`, `description`, `promotion`, `isActive`) VALUES
	(56, 1, '2013-04-10 23:10:04', '', '', 'Y'),
	(58, 1, '2013-05-26 17:01:10', '', '', 'N'),
	(59, 1, '2013-05-26 17:02:22', '', '', 'N'),
	(61, 1, '2014-01-01 17:08:48', '', '', 'Y'),
	(62, 1, '2014-01-01 17:15:59', '', '', 'Y'),
	(64, 1, '2014-01-01 18:31:48', '', '', 'Y'),
	(65, 1, '2014-01-01 18:34:39', '', '', 'Y'),
	(66, 1, '2014-01-01 18:41:28', '', '', 'Y'),
	(67, 1, '2014-01-01 18:42:51', '', '', 'Y'),
	(68, 1, '2014-01-01 18:50:08', '', '', 'Y'),
	(69, 1, '2014-01-01 18:56:06', '', '', 'Y');
/*!40000 ALTER TABLE `mealorders` ENABLE KEYS */;


-- 导出  表 food.menuitems 结构
CREATE TABLE IF NOT EXISTS `menuitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_description` varchar(2000) DEFAULT NULL,
  `isActive` char(1) DEFAULT 'Y',
  `price` decimal(10,2) NOT NULL,
  `promotion` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- 正在导出表  food.menuitems 的数据：2 rows
DELETE FROM `menuitems`;
/*!40000 ALTER TABLE `menuitems` DISABLE KEYS */;
INSERT INTO `menuitems` (`id`, `restaurant_id`, `menu_name`, `menu_description`, `isActive`, `price`, `promotion`) VALUES
	(37, 4, '鸡腿饭', '', 'Y', 10.00, ''),
	(38, 4, '狮子头饭', '', 'Y', 12.00, ''),
	(39, 4, '黑椒牛肉饭', '', 'Y', 17.00, '');
/*!40000 ALTER TABLE `menuitems` ENABLE KEYS */;


-- 导出  表 food.restaurants 结构
CREATE TABLE IF NOT EXISTS `restaurants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `description` varchar(4000) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `telephone` varchar(1000) NOT NULL,
  `isActive` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- 正在导出表  food.restaurants 的数据：~3 rows (大约)
DELETE FROM `restaurants`;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` (`id`, `name`, `description`, `address`, `telephone`, `isActive`) VALUES
	(4, '陈复新', '陈复新', '', '58956231, 33923347, 13817319495', 'Y'),
	(7, '大众快餐', '大众快餐', '', '58958635, 13045672060', 'N'),
	(8, '肯德基 ', '肯德基', '肯德基', '11111', 'N');
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;


-- 导出  表 food.users 结构
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empid` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `password` varchar(160) NOT NULL,
  `isadmin` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- 正在导出表  food.users 的数据：~12 rows (大约)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `empid`, `firstname`, `lastname`, `username`, `sex`, `password`, `isadmin`) VALUES
	(1, 160208, 'jerry', 'shen', 'jerry', 'male', '75926e095b28dd773adde5bade93e4836b1d92fc', 'Y'),
	(2, 125590, '段经理', '段', 'jeff', 'male', 'f3e731dfa293c7a83119d8aacfa41b5d2d780be9', 'N'),
	(3, 167223, 'Richard', 'Miao', 'rmiao', 'male', '47daf2864a30d74952ad4b04a5f2550244791d7d', 'Y'),
	(4, 186645, 'Wayne', 'Hong', 'whong', 'male', 'b9526479d8e75eec48092197447318d86d2194ec', 'N'),
	(5, 167211, 'Joe', 'Qiao', 'jqiao', 'male', '16a9a54ddf4259952e3c118c763138e83693d7fd', 'N'),
	(6, 188625, 'alan', 'zhang', 'alan', 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Y'),
	(7, 167419, 'vivian', 'li', 'vivian', 'female', '9dab566375668e12fbcf8db462f3703dd8009584', 'N'),
	(13, 123123, '23123', '123', '231231', 'male', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'N'),
	(14, 144339, 'Proust', 'Feng', 'proust', 'male', '0ef508f2348a0a4c9ab25afb9aa9eec3f8e19158', 'N'),
	(15, 190847, 'George', 'Qiao', 'George', 'male', 'ed54fe660af1e647843d8d1b7a89f76658ecc635', 'N'),
	(16, 1000, 'Mike', 'My', 'mike', 'male', 'a17fed27eaa842282862ff7c1b9c8395a26ac320', 'N'),
	(17, 555, 'max', 'ma', 'max', 'male', '0706025b2bbcec1ed8d64822f4eccd96314938d0', 'N');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
