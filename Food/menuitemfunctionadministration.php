<?php
include "include/isadmin.php";
include("foodmanager.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Menu Item Info.</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
    
    <body>
        <div id="wrapper">
        <?php include 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <!-- Fetch Rows -->
                        <table class="aatable">
                            <tr>
                                <th>ID</th>
                                <th>Restaurant</th>
                                <th>menu_name</th>
                                <th>menu_description</th>
                                <th>isActive</th>
                                <th>price</th>
                                <th>promotion</th>
                            </tr>
                            <?php
                            $menuitemData = getMenuItemInfo();
                            
                            for($index=0;$index < count($menuitemData);$index++){
                                $menuitem = $menuitemData[$index];
                                echo "<tr>";
                                echo "<td>".$menuitem->get_id()."</td>";
                                echo "<td>".$menuitem->get_restaurant()->get_name()."</td>";
                                echo "<td>".$menuitem->get_menu_name()."</td>";
                                echo "<td>".$menuitem->get_menu_description()."</td>";
                                echo "<td>".$menuitem->get_isActive()."</td>";
                                echo "<td>".$menuitem->get_price()."</td>";
                                echo "<td>".$menuitem->get_promotion()."</td>";
                                echo "<td><form  action='deleteMenuItemController.php' method='post'><input type='hidden' name='id' value='".$menuitem->get_id()."'/><input type='submit' value='delete'/></form>
                                            <form action='adminEditMenuItem.php' method='post'><input type='hidden' name='id' value='".$menuitem->get_id()."'/><input type='submit' value='Edit' /></form>
                                            <form action='toggleMenuController.php' method='post'><input type='hidden' name='id' value='".$menuitem->get_id()."'/><input type='submit' value='Toggle' /></form></td>";
                                echo "</tr>";
                            }

                            $restaurant_id =0;
                            $restaurantData = getRestaurantInfo($restaurant_id);
                            ?>
                            <form action="adminAddMenuItemController.php" method="post">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><select name="restaurant_id" style="width:80pt"> <?php
                            for($index=0;$index < count($restaurantData);$index++){
                                $restaurant = $restaurantData[$index];
                                 echo "<option value='".$restaurant->get_id()."'>".$restaurant->get_name()."</option>";
                                }?></select></td>
                                    <td><input type="text" size="8" name="menu_name" /></td>
                                    <td><input type="text"  size="8" name="menu_description" /></td>
                                    <td><input type="radio" name="isActive" value="Y" checked="true"/>Y<input type="radio" name="isActive" value="N"/>N</td>
                                    <td><input type="text"  size="8" name="price" /></td>
                                    <td><textarea cols="8" name="promotion"></textarea></td>
                                    <td><input type="submit" value="add" /></td>
                                </tr>
                            </form>
                        </table>
                    </div>
                    <!-- end div#welcome -->			
                    
                </div>
                <!-- end div#content -->
                <div id="sidebar">
                    <!--ul-->
                        <?php if ($_SESSION['isAdmin'] ){
                                include 'include/adminnav.php';
                                }else{
                                  include 'include/usernav.php';
                                } ?>
                        <!-- end navigation -->
                            <?php include 'include/updates.php'; ?>
                        <!-- end updates -->
                    <!--/ul-->
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
