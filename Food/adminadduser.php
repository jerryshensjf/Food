<?php
include_once "include/isadmin.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Admin add user</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
   
    <body>
        <div id="wrapper">
        <?php include 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <form action="adminAddUserController.php" method="post">
                        <h2><?php
                                if (isset($_SESSION['errorMessage'])) {
									echo $_SESSION['errorMessage'];
									unset($_SESSION['errorMessage']);
								}
                                ?></h2>
                        <!-- Fetch Rows -->
                        <table class="aatable">
                                <tr><td colspan='2'> Admin add user </td></tr>
                                <tr><td>EmpID:</td><td><input type='text' name='empid' value=''/></td></tr>
                                <tr><td>User Name:</td><td><input type='text' name='username' value=''/></td></tr>
                                <tr><td>First Name:</td><td><input type='text' name='firstname' value=''/></td></tr>
                                <tr><td>Last Name:</td><td><input type='text' name='lastname' value=''/></td></tr>
                                <tr><td>Sex:</td><td><input type='radio' name='sex' value='male' checked='true'/>Male<input type='radio' name='sex' value='female'/>Female</td></tr>
                                <tr><td>Password</td><td><input type='password' name='password' value=''/></td></tr>
                                <tr><td>Confirm Password</td><td><input type='password' name='confirmpassword' value=''/></td></tr>
                                <tr><td colspan='2'><input type='submit' value='Register'/>&nbsp;<input type='button' value='Back' onClick="window.location.href='adminhomepage.php';" /></td></tr>
                        </table>
                        </form>
                    </div>
                    <!-- end div#welcome -->			
                    
                </div>
                <!-- end div#content -->
                <div id="sidebar">
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
