<?php
include_once "include/isadmin.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Menu Item Info.</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
    <?php
            include_once("foodmanager.php");
            $id=0;
            if (!isset($_SESSION)){
              session_start();
            }
            if(isset($_POST['id'])){
                $id = $_POST['id'];
            }
    ?>

    <body>
        <div id="wrapper">
        <?php include_once 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <form action="adminEditMenuItemController.php" method="post">
                        <!-- Fetch Rows -->
                        <table class="aatable">
                            <?php
                            $menuItemData = getMenuItemInfoById($id);

                            for($index=0;$index < count($menuItemData);$index++){
                                $menuItem = $menuItemData[$index];
                                echo "<tr><td colspan='2'> Edit Menu Item<input type='hidden' name='id' value='".$menuItem->get_id()."'></td></tr>";
                                echo "<tr><td>restaurant_id:</td><td><input type='text' name='restaurant_id' value='".$menuItem->get_restaurant_id()."'/></td></tr>";
                                echo "<tr><td>menu_name:</td><td><input type='text' name='menu_name' value='".$menuItem->get_menu_name()."'/></td></tr>";
                                echo "<tr><td>menu_description:</td><td><input type='text' name='menu_description' value='".$menuItem->get_menu_description()."'/></td></tr>";
                                echo "<tr><td>price:</td><td><input type='text' name='price' value='".$menuItem->get_price()."'/></td></tr>";
                                echo "<tr><td>promotion:</td><td><input type='text' name='promotion' value='".$menuItem->get_promotion()."'/></td></tr>";
                                echo "<tr><td>IsActive:</td><td>".$menuItem->get_isActive()."</td></tr>";
                                echo "<tr><td colspan='2'><input type='submit' value='Edit'/>&nbsp;<input type='button' value='Cancel' /></td></tr>";
                            }
                            ?>
                        </table>
                        </form>
                    </div>
                    <!-- end div#welcome -->

                </div>
                <!-- end div#content -->
                <div id="sidebar">
                    <!--ul-->
                        <?php if ($_SESSION['isAdmin'] ){
                                include 'include/adminnav.php';
                                }else{
                                  include 'include/usernav.php';
                                }?>
                        <!-- end navigation -->
                            <?php include 'include/updates.php'; ?>
                        <!-- end updates -->
                    <!--/ul-->
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include_once 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
