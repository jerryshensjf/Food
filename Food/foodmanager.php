<?php
/*
This file contains the functions
that performs the DB operation. The
Database properties are taken from the
session.
*/
include_once("domain/user.php");
include_once("domain/restaurant.php");
include_once("domain/menuitem.php");
include_once("domain/mealorder.php");
include_once("domain/mealorderitem.php");
include_once("conf/conf.php");

/*
DB Initialization method.
Returns the connection variable.
*/
function initDB() {
    $dbConf = new FoodConf();
    $databaseURL = $dbConf->get_databaseURL();
    $databaseUName = $dbConf->get_databaseUName();
    $databasePWord = $dbConf->get_databasePWord();
    $databaseName = $dbConf->get_databaseName();

     $connection = mysqli_connect($databaseURL,$databaseUName,$databasePWord,$databaseName) or die ("Error while connecting to host");
     $db = mysqli_select_db($connection,$databaseName) or die ("Error while connecting to database");
     $query = "set names utf8";
     mysqli_query($connection,$query);
     //mysqli_close($connection);
     return $connection;
}

/*
DB Closing method.
Pass the connection variable
obtained through initDB().
*/
function closeDB($connection) {
    mysqli_close($connection);
}


function getUserInfo($id) {
    $connection = initDB();
    $query;

    if($id == 0) {
        $query = "SELECT * FROM users";
    }
    else {
        $query = "SELECT * FROM users WHERE ID='".$id."'";
    }


    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $userData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $userName = $row['username'];
        $empId = $row['empid'];
        $firstName = $row['firstname'];
        $lastName = $row['lastname'];
        $sex = $row['sex'];
        $password = $row['password'];
        $isAdmin = $row['isadmin'];

        //Build the user object
        $user = new User();
        $user->set_id($id);
        $user->set_empid($empId);
        $user->set_username($userName);
        $user->set_firstname($firstName);
        $user->set_lastname($lastName);
        $user->set_sex($sex);
        $user->set_password($password);
        $user->set_isadmin($isAdmin);

        //Build the Flight object array
        $userData[$lineId] = $user;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $userData;
}


function editUser($user) {
    $connection = initDB();
    $query;

    if(isset($user)) {
        $query0 = "SELECT count(*) as countNum FROM users where id<>'".$user->get_id()."' and  empid='".$user->get_empid()."' or username='".$user->get_username()."' and id<>'".$user->get_id()."';";

        $result0 = mysqli_query($connection,$query0);
        $countArr =  mysqli_fetch_assoc($result0);
        $count = $countArr['countNum'];

        if (isset($count)&&$count>0) {
            session_start();
            $_SESSION['errorMessage']="User already exists.";
            closeDB($connection);
            return false;
        }
        else {
            $query = "update users set username='".$user->get_username()."',  firstname='".$user->get_firstname()."', lastname='".$user->get_lastname()."',sex='".$user->get_sex()."',empid='".$user->get_empid()."' where id='".$user->get_id()."';";
            //echo $query;
            $result = mysqli_query($connection,$query);
            // or die ("Query Failed ".mysqli_error());
            closeDB($connection);
            return true;
        }
    }
    return false;
}

function login($user) {
    $connection = initDB();
    $query;
    $query2;

    if(isset($user)) {
        $query = "SELECT * FROM users where empid='".$user->get_empid()."' and password='".sha1($user->get_password())."'";
        $query2 = "SELECT count(*) as countNum FROM users where empid='".$user->get_empid()."' and password='".sha1($user->get_password())."'";
    }
    else {
        echo 'user is empty';
    }


    $result = mysqli_query($connection,$query) or die ("Query Failed ".mysqli_error());
    $result2 = mysqli_query($connection,$query2);
    $countArr =  mysqli_fetch_assoc($result2);
    $count = $countArr['countNum'];
    $arr = mysqli_fetch_assoc($result);

    closeDB($connection);
    if (isset($arr)&&$count==1) {
        session_start();
        $_SESSION['login']=true;
        $_SESSION['userData']=json_encode($arr);
        $_SESSION['isAdmin']=$arr['isadmin']=='Y';
        return true;
    }else {
        return false;
    }
}

function loginViaUserNamePassword($user) {
    $connection = initDB();
    $query;
    $query2;

    if(isset($user)) {
        $query = "SELECT * FROM users where username='".$user->get_username()."' and password='".sha1($user->get_password())."'";
        $query2 = "SELECT count(*) as countNum FROM users where username='".$user->get_username()."' and password='".sha1($user->get_password())."'";
    }
    else {
        echo 'user is empty';
    }


    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $result2 = mysqli_query($connection,$query2);
    $countArr =  mysqli_fetch_assoc($result2);
    $count = $countArr['countNum'];
    $arr = mysqli_fetch_assoc($result);

    closeDB($connection);
    if (isset($arr)&&$count==1) {
        session_start();
        $_SESSION['login']=true;
        $_SESSION['userData']=json_encode($arr);
        $_SESSION['isAdmin']=$arr['isadmin']=='Y';
        return true;
    }else {
        return false;
    }
}

function changePassword($user,$newpassword,$confirmnewpassword) {
    if ($newpassword!=$confirmnewpassword) {
        session_start();
        $_SESSION['errorMessage']="New password not match.";
        return false;
    }
    $connection = initDB();
    $query;

    if(isset($user)) {
        $query = "SELECT count(*) as countNum FROM users where id='".$user->get_id()."' and password='".$user->get_password()."';";
    }

    $result = mysqli_query($connection,$query);
    $countArr =  mysqli_fetch_assoc($result);
    $count = $countArr['countNum'];

    if (isset($count)&&$count==1) {
        $query2 = "update users set password='".sha1($newpassword)."' where id='".$user->get_id()."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update password error.";
            closeDB($connection);
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="Old password error.";
        closeDB($connection);
        return false;
    }
}

function adminChangeUserPassword($user,$confirmnewpassword) {
    if ($user->get_password()!=$confirmnewpassword) {
        session_start();
        $_SESSION['errorMessage']="New password not match.";
        return false;
    }
    $connection = initDB();
    $query;

    if(isset($user)) {
        $query = "SELECT count(*) as countNum FROM users where id='".$user->get_id()."';";
    }

    $result = mysqli_query($connection,$query);
    $countArr =  mysqli_fetch_assoc($result);
    $count = $countArr['countNum'];

    if (isset($count)&&$count==1) {
        $query2 = "update users set password='".sha1($user->get_password())."' where id='".$user->get_id()."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update password error.";
            closeDB($connection);
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="User did not exist.";
        closeDB($connection);
        return false;
    }
}

function toggleAdmin($user) {
    $connection = initDB();
    $query;

    if(isset($user)) {
        $query = "SELECT id, isadmin from users where id='".$user->get_id()."';";
    }

    $result = mysqli_query($connection,$query);
    $arr =  mysqli_fetch_assoc($result);
    $isAdmin = $arr['isadmin'];
    $isNewAdmin;
    session_start();
    $userData = json_decode($_SESSION['userData'],true);

    if ($isAdmin=='N' || $userData['id']==$user->get_id()) {
        $isNewAdmin='Y';
    }else if($isAdmin=='Y') {
            $isNewAdmin= 'N';
        }else {
            $isNewAdmin= 'N';
        }

    if (isset($isNewAdmin)) {
        $query2 = "update users set isadmin='".$isNewAdmin."' where id='".$user->get_id()."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update isAdmin error.";
            closeDB($connection);
            return false;
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="User did not exist.";
        closeDB($connection);
        return false;
    }
}

function adminDeleteUser($id) {
    $connection = initDB();
    $query;

    if(isset($id)) {
        $query = "delete from  users where id='".$id."';";
    }
    else {
        echo "The id is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    closeDB($connection);
    if ($result==false) {
        echo "delete wrong!";
        return false;
    }else {
        return true;
    }
}


function registerUser($user,$confirmpassword) {
    if ($user->get_password()!=$confirmpassword) {
        session_start();
        $_SESSION['errorMessage']="Password not match.";
        return false;
    }
    $connection = initDB();
    $query;

    if(isset($user)) {
        $query = "SELECT count(*) as countNum FROM users where empid='".$user->get_empid()."' or username='".$user->get_username()."';";
    }

    $result = mysqli_query($connection,$query);
    $countArr =  mysqli_fetch_assoc($result);
    $count = $countArr['countNum'];

    if (isset($count)&&$count==1) {
        session_start();
        $_SESSION['errorMessage']="User already exists.";
        closeDB($connection);
        return false;
    }else {
        $query2 = "insert into users (empid,username,firstname,lastname,sex,password) values ('".$user->get_empid()."','".$user->get_username()."','".$user->get_firstname()."','".$user->get_lastname()."','".$user->get_sex()."','".sha1($user->get_password())."');";
        //echo $query2;
        $result2 = mysqli_query($connection,$query2);
        if ($result2== true) {
            closeDB($connection);
            return true;
        } else {
            session_start();
            $_SESSION['errorMessage']="Register error.";
            closeDB($connection);
            return false;
        }
    }
}



function getRestaurantInfo($id) {
    $connection = initDB();
    $query;

    if($id == 0) {
        $query = "SELECT * FROM restaurants";
    }
    else {
        $query = "SELECT * FROM restaurants WHERE ID='".$id."'";
    }


    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $restaurantData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $name = $row['name'];
        $address = $row['address'];
        $telephone = $row['telephone'];
        $description = $row['description'];
        $isActive = $row['isActive'];

        //Build the user object
        $restaurant = new Restaurant();
        $restaurant->set_id($id);
        $restaurant->set_name($name);
        $restaurant->set_address($address);
        $restaurant->set_telephone($telephone);
        $restaurant->set_description($description);
        $restaurant->set_isactive($isActive);

        //Build the Flight object array
        $restaurantData[$lineId] = $restaurant;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $restaurantData;
}

function getUserRestaurantInfo() {
    $connection = initDB();
    $query;

    $query = "SELECT * FROM restaurants where isActive='Y'";

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $restaurantData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $name = $row['name'];
        $address = $row['address'];
        $telephone = $row['telephone'];
        $description = $row['description'];
        $isActive = $row['isActive'];

        //Build the user object
        $restaurant = new Restaurant();
        $restaurant->set_id($id);
        $restaurant->set_name($name);
        $restaurant->set_address($address);
        $restaurant->set_telephone($telephone);
        $restaurant->set_description($description);
        $restaurant->set_isactive($isActive);

        //Build the Flight object array
        $restaurantData[$lineId] = $restaurant;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $restaurantData;
}


function addRestaurant($restaurant) {
    $connection = initDB();
    $query;

    if(isset($restaurant)) {
        $query = "insert into restaurants (name, telephone, address, description,isactive) values ('".$restaurant->get_name()."','".$restaurant->get_telephone()."','".$restaurant->get_address()."','".$restaurant->get_description()."','".$restaurant->get_isactive()."');";
    }
    else {
        echo "The user is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    if ($result==false) {
        echo "insert wrong!";
        return false;
    }else {
        return true;
    }
}

function editRestaurant($restaurant) {
    $connection = initDB();
    $query;

    if(isset($restaurant)) {
        $query = "update restaurants set name='".$restaurant->get_name()."',  description='".$restaurant->get_description()."', address='".$restaurant->get_address()."',telephone='".$restaurant->get_telephone()."' where id='".$restaurant->get_id()."';";
    //echo $query;
    }

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    closeDB($connection);
}

function deleteRestaurant($id) {
    $connection = initDB();
    $query;

    if(isset($id)) {
        $query = "delete from  restaurants where id='".$id."';";
    }
    else {
        echo "The id is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    if ($result==false) {
        echo "delete wrong!";
        return false;
    }else {
        return true;
    }
}

function toggleRestaurant($restaurant) {
    $connection = initDB();
    $query;

    if(isset($restaurant)) {
        $query = "SELECT id, isActive from restaurants where id='".$restaurant->get_id()."';";
    }

    $result = mysqli_query($connection,$query);
    $arr =  mysqli_fetch_assoc($result);
    $isActive = $arr['isActive'];
    $isNewActive;

    if ($isActive=='N' ) {
        $isNewActive='Y';
    }else if($isActive=='Y') {
            $isNewActive= 'N';
        }else {
            $isNewActive= 'N';
        }

    if (isset($isNewActive)) {
        $query2 = "update restaurants set isActive='".$isNewActive."' where id='".$restaurant->get_id()."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update isAdmin error.";
            closeDB($connection);
            return false;
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="User did not exist.";
        closeDB($connection);
        return false;
    }
}

function getMenuItemInfo() {
    $connection = initDB();
    $query;

    $query = "SELECT  menuitems.id as id,restaurant_id,menu_name,menu_description,menuitems.isActive,price,menuitems.promotion,restaurants.id as rest_id,restaurants.name,restaurants.description,address,telephone,restaurants.isactive FROM menuitems, restaurants where menuitems.restaurant_id=restaurants.id";

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $menuItemData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $restaurant_id = $row['restaurant_id'];
        $menu_name = $row['menu_name'];
        $menu_description = $row['menu_description'];
        $isActive = $row['isActive'];
        $price = $row['price'];
        $promotion= $row['promotion'];

        $restaurant_name=$row['name'];
        $restaurant_description=$row['description'];
        $restaurant_address=$row['address'];
        $restaurant_telephone=$row['telephone'];
        $restaurant_isactive=$row['isactive'];

        //Build the user object
        $restaurant = new Restaurant();
        $restaurant->set_id($restaurant_id);
        $restaurant->set_name($restaurant_name);
        $restaurant->set_address($restaurant_address);
        $restaurant->set_telephone($restaurant_telephone);
        $restaurant->set_description($restaurant_description);
        $restaurant->set_isactive($restaurant_isactive);

        $menuItem = new MenuItem();
        $menuItem->set_id($id);
        $menuItem->set_restaurant_id($restaurant_id);
        $menuItem->set_restaurant($restaurant);
        $menuItem->set_menu_name($menu_name);
        $menuItem->set_menu_description($menu_description);
        $menuItem->set_isActive($isActive);
        $menuItem->set_price($price);
        $menuItem->set_promotion($promotion);

        //Build the Flight object array
        $menuItemData[$lineId] = $menuItem;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $menuItemData;
}

function getMenuItemInfoById($id) {
    $connection = initDB();
    $query;

    $query = "SELECT  menuitems.id as id,restaurant_id,menu_name,menu_description,menuitems.isActive,price,menuitems.promotion,restaurants.id as rest_id,restaurants.name,restaurants.description,address,telephone,restaurants.isactive FROM menuitems, restaurants where menuitems.restaurant_id=restaurants.id and menuitems.id='".$id."';";

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $menuItemData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $restaurant_id = $row['restaurant_id'];
        $menu_name = $row['menu_name'];
        $menu_description = $row['menu_description'];
        $isActive = $row['isActive'];
        $price = $row['price'];
        $promotion= $row['promotion'];

        $restaurant_name=$row['name'];
        $restaurant_description=$row['description'];
        $restaurant_address=$row['address'];
        $restaurant_telephone=$row['telephone'];
        $restaurant_isactive=$row['isactive'];

        //Build the user object
        $restaurant = new Restaurant();
        $restaurant->set_id($restaurant_id);
        $restaurant->set_name($restaurant_name);
        $restaurant->set_address($restaurant_address);
        $restaurant->set_telephone($restaurant_telephone);
        $restaurant->set_description($restaurant_description);
        $restaurant->set_isactive($restaurant_isactive);

        $menuItem = new MenuItem();
        $menuItem->set_id($id);
        $menuItem->set_restaurant_id($restaurant_id);
        $menuItem->set_restaurant($restaurant);
        $menuItem->set_menu_name($menu_name);
        $menuItem->set_menu_description($menu_description);
        $menuItem->set_isActive($isActive);
        $menuItem->set_price($price);
        $menuItem->set_promotion($promotion);

        //Build the Flight object array
        $menuItemData[$lineId] = $menuItem;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $menuItemData;
}

function getMenuItemInfoByRestaurantId($restaurant_id) {
    $connection = initDB();
    $query;

    $query = "SELECT  menuitems.id as id,restaurant_id,menu_name,menu_description,menuitems.isActive,price,menuitems.promotion,restaurants.id as rest_id,restaurants.name,restaurants.description,address,telephone,restaurants.isactive FROM menuitems, restaurants where menuitems.restaurant_id=restaurants.id and menuitems.isActive='Y' and menuitems.restaurant_id='".$restaurant_id."';";

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    $menuItemData;
    $lineId = 0;

    while($row = mysqli_fetch_array($result)) {

        $id = $row['id'];
        $restaurant_id = $row['restaurant_id'];
        $menu_name = $row['menu_name'];
        $menu_description = $row['menu_description'];
        $isActive = $row['isActive'];
        $price = $row['price'];
        $promotion= $row['promotion'];

        $restaurant_name=$row['name'];
        $restaurant_description=$row['description'];
        $restaurant_address=$row['address'];
        $restaurant_telephone=$row['telephone'];
        $restaurant_isactive=$row['isactive'];

        //Build the user object
        $restaurant = new Restaurant();
        $restaurant->set_id($restaurant_id);
        $restaurant->set_name($restaurant_name);
        $restaurant->set_address($restaurant_address);
        $restaurant->set_telephone($restaurant_telephone);
        $restaurant->set_description($restaurant_description);
        $restaurant->set_isactive($restaurant_isactive);

        $menuItem = new MenuItem();
        $menuItem->set_id($id);
        $menuItem->set_restaurant_id($restaurant_id);
        $menuItem->set_restaurant($restaurant);
        $menuItem->set_menu_name($menu_name);
        $menuItem->set_menu_description($menu_description);
        $menuItem->set_isActive($isActive);
        $menuItem->set_price($price);
        $menuItem->set_promotion($promotion);

        //Build the Flight object array
        $menuItemData[$lineId] = $menuItem;
        $lineId = $lineId + 1;
    }
    closeDB($connection);
    return $menuItemData;
}

function addMenuItem($menuItem) {
    $connection = initDB();
    $query;

    if(isset($menuItem)) {
        $query = "insert into menuitems (restaurant_id, menu_name, menu_description, isActive,price,promotion) values ('".$menuItem->get_restaurant_id()."','".$menuItem->get_menu_name()."','".$menuItem->get_menu_description()."','".$menuItem->get_isActive()."','".$menuItem->get_price()."','".$menuItem->get_promotion()."');";
    }
    else {
        echo "The user is empty!";
        return false;
    }

    // echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    if ($result==false) {
        echo "insert wrong!";
        return false;
    }else {
        return true;
    }
}

function deleteMenuItem($id) {
    $connection = initDB();
    $query;

    if(isset($id)) {
        $query = "delete from  menuitems where id='".$id."';";
    }
    else {
        echo "The id is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());

    if ($result==false) {
        echo "delete wrong!";
        return false;
    }else {
        return true;
    }
}

function toggleMenuItem($menuItem) {
    $connection = initDB();
    $query;

    if(isset($menuItem)) {
        $query = "SELECT id, isActive from menuitems where id='".$menuItem->get_id()."';";
    }

    $result = mysqli_query($connection,$query);
    $arr =  mysqli_fetch_assoc($result);
    $isActive = $arr['isActive'];
    $isNewActive;

    if ($isActive=='N' ) {
        $isNewActive='Y';
    }else if($isActive=='Y') {
            $isNewActive= 'N';
        }else {
            $isNewActive= 'N';
        }

    if (isset($isNewActive)) {
        $query2 = "update menuitems set isActive='".$isNewActive."' where id='".$menuItem->get_id()."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update isActive error.";
            closeDB($connection);
            return false;
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="Menu item did not exist.";
        closeDB($connection);
        return false;
    }
}

function editMenuItem($menuItem) {
    $connection = initDB();
    $query;

    if(isset($menuItem)) {
        $query = "update menuitems set restaurant_id='".$menuItem->get_restaurant_id()."',  menu_name='".$menuItem->get_menu_name()."', menu_description='".$menuItem->get_menu_description()."',price='".$menuItem->get_price()."',promotion='".$menuItem->get_promotion()."' where id='".$menuItem->get_id()."';";
    //echo $query;
    }

    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    closeDB($connection);
}

function addMealOrder($mealorder, $mealorderitem){
 $connection = initDB();
    $query;

    if(isset($mealorder)) {
        $query = "insert into mealorders (user_id, order_time, description, promotion) values ('".$mealorder->get_user_id()."','".$mealorder->get_order_time()."','".$mealorder->get_description()."','".$mealorder->get_promotion()."');";
    }
    else {
        echo "The meal order is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    $mealorder_id = mysqli_insert_id($connection);

    if(isset($mealorder)) {
    $mealorderitem->set_mealorder_id($mealorder_id);

    $query1 = "insert into mealorderitems (mealorder_id, menuitem_id,price, amount) values ('".$mealorderitem->get_mealorder_id()."','".$mealorderitem->get_menuitem_id()."','".$mealorderitem->get_price()."','".$mealorderitem->get_amount()."');";
    //echo $query1;
    $result1 = mysqli_query($connection,$query1);
    }
    else {
        echo "The meal order item is empty!";
        return false;
    }
    if ($result==false) {
        echo "insert meal order wrong!";
        return false;
    }else if ($result1==false){
        echo "insert meal order item wrong!";
        return false;
    }else{
        return true;
    }
}

function getMealOrderInfo($user_id){
  $connection = initDB();
    $query;

    if(isset($user_id)) {
        date_default_timezone_set("PRC");
        $starttime=date("Y-m-d");
        $endtime=date("Y-m-d",(time()+3600*24));

        $query = "SELECT mealorders.id as mealorder_id, user_id, order_time,mealorders.description as mealorder_description,mealorders.promotion as mealorder_promotion,mealorders.isActive as mealorder_isActive,mealorderitems.id as mealorderitem_id,menuitem_id,mealorderitems.price as mealorderitem_price,amount,menu_name,name FROM mealorders, mealorderitems,menuitems, restaurants where mealorderitems.mealorder_id=mealorders.id and mealorderitems.menuitem_id=menuitems.id and menuitems.restaurant_id=restaurants.id and user_id='".$user_id."' and order_time<'".$endtime."' and order_time>='".$starttime."' and mealorders.isActive='Y';";
        //echo $query;
        $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $res_array = Array();
    for ($count=0; $row = mysqli_fetch_assoc($result); $count++) {
     $res_array[$count] = $row;
    }
    closeDB($connection);
    return $res_array;
    }
    else {
        closeDB($connection);
        echo "Get Meal Order Info fail, the user id is not set.";
        return null;
    }
}

function getMealOrderInfoUnActive($user_id){
  $connection = initDB();
    $query;

    if(isset($user_id)) {
        date_default_timezone_set("PRC");
        $starttime=date("Y-m-d");
        $endtime=date("Y-m-d",(time()+3600*24));

        $query = "SELECT mealorders.id as mealorder_id, user_id, order_time,mealorders.description as mealorder_description,mealorders.promotion as mealorder_promotion,mealorders.isActive as mealorder_isActive,mealorderitems.id as mealorderitem_id,menuitem_id,mealorderitems.price as mealorderitem_price,amount,menu_name,name FROM mealorders, mealorderitems,menuitems, restaurants where mealorderitems.mealorder_id=mealorders.id and mealorderitems.menuitem_id=menuitems.id and menuitems.restaurant_id=restaurants.id and user_id='".$user_id."' and order_time<'".$endtime."' and order_time>='".$starttime."' and mealorders.isActive='N';";
        //echo $query;
        $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $res_array = Array();
    for ($count=0; $row = mysqli_fetch_assoc($result); $count++) {
     $res_array[$count] = $row;
    }
    closeDB($connection);
    return $res_array;
    }
    else {
        closeDB($connection);
        echo "Get Meal Order Info fail, the user id is not set.";
        return null;
    }
}

function getMealOrderInfoAll(){
  $connection = initDB();
    $query;


        date_default_timezone_set("PRC");
        $starttime=date("Y-m-d");
        $endtime=date("Y-m-d",(time()+3600*24));

        $query = "SELECT mealorders.id as mealorder_id, user_id, order_time,mealorders.description as mealorder_description,mealorders.promotion as mealorder_promotion,mealorders.isActive as mealorder_isActive,mealorderitems.id as mealorderitem_id,menuitem_id,mealorderitems.price as mealorderitem_price,amount,menu_name,name,users.id,username,empid FROM mealorders, mealorderitems,menuitems, restaurants,users where mealorderitems.mealorder_id=mealorders.id and mealorderitems.menuitem_id=menuitems.id and menuitems.restaurant_id=restaurants.id and user_id=users.id and order_time<'".$endtime."' and order_time>='".$starttime."' and mealorders.isActive='Y';";
        //echo $query;
        $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $res_array = Array();
    for ($count=0; $row = mysqli_fetch_assoc($result); $count++) {
     $res_array[$count] = $row;
    }
    closeDB($connection);
    return $res_array;
}

function getMealOrderInfoHistory(){
  $connection = initDB();
    $query;


        date_default_timezone_set("PRC");
        $starttime=date("Y-m-d");
        $endtime=date("Y-m-d",(time()+3600*24));

        $query = "SELECT mealorders.id as mealorder_id, user_id, order_time,mealorders.description as mealorder_description,mealorders.promotion as mealorder_promotion,mealorders.isActive as mealorder_isActive,mealorderitems.id as mealorderitem_id,menuitem_id,mealorderitems.price as mealorderitem_price,amount,menu_name,name,users.id,username,empid FROM mealorders, mealorderitems,menuitems, restaurants,users where mealorderitems.mealorder_id=mealorders.id and mealorderitems.menuitem_id=menuitems.id and menuitems.restaurant_id=restaurants.id and user_id=users.id order by order_time desc;";
        //echo $query;
        $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $res_array;
    for ($count=0; $row = mysqli_fetch_assoc($result); $count++) {
     $res_array[$count] = $row;
    }
    closeDB($connection);
    return $res_array;
}

function deleteMealOrder($id) {
    $connection = initDB();
    $query;

    if(isset($id)) {
        $query = "delete from  mealorders where id='".$id."';";
    }
    else {
        echo "The id is empty!";
        return false;
    }

    //echo $query;
    $result = mysqli_query($connection,$query);
    // or die ("Query Failed ".mysqli_error());
    $query2 = "delete from  mealorderitems where mealorder_id='".$id."';";

    $result2 = mysqli_query($connection,$query2);

    if ($result==false) {
        echo "delete mealorder wrong!";
        return false;
    }else if ($result2==false){
        echo "delete mealorderitem wrong!";
        return false;
    }else {
        return true;
    }
}

function toggleMealOrder($id) {
    $connection = initDB();
    $query;

    if(isset($id)) {
        $query = "SELECT id, isActive from mealorders where id='".$id."';";
    }

    $result = mysqli_query($connection,$query);
    $arr =  mysqli_fetch_assoc($result);
    $isActive = $arr['isActive'];
    $isNewActive;

    if ($isActive=='N' ) {
        $isNewActive='Y';
    }else if($isActive=='Y') {
            $isNewActive= 'N';
        }else {
            $isNewActive= 'N';
        }

    if (isset($isNewActive)) {
        $query2 = "update mealorders set isActive='".$isNewActive."' where id='".$id."';";
        $result2 = mysqli_query($connection,$query2);
        if ($result2==true) {
            closeDB($connection);
            return true;
        }else {
            session_start();
            $_SESSION['errorMessage']="Update isActive error.";
            closeDB($connection);
            return false;
        }

    }else {
        session_start();
        $_SESSION['errorMessage']="Meal order did not exist.";
        closeDB($connection);
        return false;
    }
}
?>
