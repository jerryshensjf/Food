<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
include "foodmanager.php";

$menuItemData = getMenuItemInfoById($_POST['menuitem_id']);
$menuitem = $menuItemData[0];

$userid = (int)$_POST['user_id'];
$userArr = getUserInfo(userid);
$user = $userArr[0];

$mealorder = new MealOder();
$mealorder->set_user_id($userid);
$mealorder->set_user($user);
date_default_timezone_set("PRC");
$mealorder->set_order_time(date("Y-m-d H:i:s"));
$mealorder->set_promotion($menuitem->get_promotion());
$mealorder->set_description($menuitem->get_menu_description());

$mealorderitem = new MealOrderItem();
$amount=(int)$_POST['amount'];
if(!is_int($amount)){
    $amount=1;
}
$mealorderitem->set_amount($amount);
$mealorderitem->set_menuitem_id($menuitem->get_id());
$mealorderitem->set_price($menuitem->get_price());



if (addMealOrder($mealorder, $mealorderitem)) {
    Header("Location:viewAllMealOrder.php");
} else {
    header("Cache-control: private, must-revalidate");
}
?>
