<?php
include "include/islogin.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>User Info.</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
    <?php
            include("foodmanager.php");
            $id=0;
            // echo $_SESSION['userData'];
            if(isset($_SESSION['userData'])){
                    $userData=json_decode((string)$_SESSION['userData'],true);
                    $id = $userData['id'];
        }
    ?>
    
    <body>
        <div id="wrapper">
        <?php include 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <form action="editUserController.php" method="post">
                            <h2><?php
								if (isset($_SESSION['errorMessage'])) {
									echo $_SESSION['errorMessage'];
									unset($_SESSION['errorMessage']);
								}
                                ?></h2>
                        <!-- Fetch Rows -->
                        <table class="aatable">
                            <?php
                            $userData = getUserInfo($id);
                            
                            for($index=0;$index < count($userData);$index++){
                                $user = $userData[$index];
                                echo "<tr><td colspan='2'> Edit User<input type='hidden' name='id' value='".$user->get_id()."'></td></tr>";
                                echo "<tr><td>EmpID:</td><td><input type='text' name='empid' value='".$user->get_empid()."'/></td></tr>";
                                echo "<tr><td>User Name:</td><td><input type='text' name='username' value='".$user->get_username()."'/></td></tr>";
                                echo "<tr><td>First Name:</td><td><input type='text' name='firstname' value='".$user->get_firstname()."'/></td></tr>";
                                echo "<tr><td>Last Name:</td><td><input type='text' name='lastname' value='".$user->get_lastname()."'/></td></tr>";
                                echo "<tr><td>Sex:</td><td><input type='text' name='sex' value='".$user->get_sex()."'/></td></tr>";
                                echo "<tr><td>IsAdmin:</td><td>".$user->get_isadmin()."</td></tr>";
                                echo "<tr><td colspan='2'><input type='submit' value='Edit'/>&nbsp;<input type='button' value='Cancel' onclick='window.location.href=\"userhomepage.php\"'/></td></tr>";
                            }
                            ?>
                        </table>
                        </form>
                    </div>
                    <!-- end div#welcome -->			
                    
                </div>
                <!-- end div#content -->
                <div id="sidebar">
                    <!--ul-->
                        <?php if ($_SESSION['isAdmin'] ){
                                include 'include/adminnav.php';
                                }else{
                                  include 'include/usernav.php';
                                } ?>
                        <!-- end navigation -->
                            <?php include 'include/updates.php'; ?>
                        <!-- end updates -->
                    <!--/ul-->
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
