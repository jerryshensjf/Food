<?php
/*
This is a Flight helper class. Instance of
this class can store flight information.
*/

class User{
    private $id;
    private $empid;
    private $username;
    private $firstname;
    private $lastname;
    private $password;
    private $sex;
    private $isadmin;

    public function get_id() {
        return $this->id;
    }

    public function set_id($id) {
        $this->id = $id;
    }

    public function get_empid() {
        return $this->empid;
    }

    public function set_empid($empid) {
        $this->empid = $empid;
    }

    public function get_username() {
        return $this->username;
    }

    public function set_username($username) {
        $this->username = $username;
    }

    public function get_firstname() {
        return $this->firstname;
    }

    public function set_firstname($firstname) {
        $this->firstname = $firstname;
    }

    public function get_lastname() {
        return $this->lastname;
    }

    public function set_lastname($lastname) {
        $this->lastname = $lastname;
    }

    public function get_password() {
        return $this->password;
    }

    public function set_password($password) {
        $this->password = $password;
    }

    public function get_sex() {
        return $this->sex;
    }

    public function set_sex($sex) {
        $this->sex = $sex;
    }

    public function get_isadmin() {
        return $this->isadmin;
    }

    public function set_isadmin($isadmin) {
        $this->isadmin = $isadmin;
    }

    // The constructor, duh!
    function __construct(){
    }
}
?>