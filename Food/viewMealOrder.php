<?php
include "include/islogin.php";
include("foodmanager.php");
$userid=0;
if (isset($_SESSION['userData'])) {
	$userData = json_decode($_SESSION['userData'],true);
	$userid = $userData['id'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>View Order Info.</title>
        <link href="css/default.css" rel="stylesheet" type="text/css" />
    </head>
    
    <body>
        <div id="wrapper">
        <?php include 'include/header.php'; ?>
            <!-- end div#header -->
            <div id="page">
                <div id="content">
                    <div id="welcome">
                        <!-- Fetch Rows -->
                        <table class="aatable">
                            <tr>
                                <th>Meal Order ID</th>
                                <th>Restaurant</th>
                                <th>menu_name</th>
                                <th>menu_description</th>
                                <th>price</th>
                                <th>promotion</th>
                                <th>amount</th>
                                <th>Operation</th>
                            </tr>
                            <?php
                            $mealOrderInfo = getMealOrderInfo($userid);
                            
                            for($index=0;$index < count($mealOrderInfo);$index++){
                                $mealOrder = $mealOrderInfo[$index];
                                echo "<tr>";
                                echo "<td>".$mealOrder['mealorder_id']."</td>";
                                echo "<td>".$mealOrder['name']."</td>";
                                echo "<td>".$mealOrder['menu_name']."</td>";
                                echo "<td>".$mealOrder['mealorder_description']."</td>";
                                echo "<td>".$mealOrder['mealorderitem_price']."</td>";
                                echo "<td>".$mealOrder['mealorder_promotion']."</td>";
                                echo "<td>".$mealOrder['amount']."</td>";
                                echo "<td><form  action='deleteMealOrderController.php' method='post'><input type='hidden' name='id' value='".$mealOrder['mealorder_id']."'/><input type='submit' value='delete'/></form>
                                            <form action='toggleMealOrderController.php' method='post'><input type='hidden' name='id' value='".$mealOrder['mealorder_id']."'/><input type='submit' value='Toggle Meal Order' /></form></td>";
                                echo "</tr>";
                            }
                            ?>
                        </table>
                    </div>
                    <!-- end div#welcome -->			
                    
                </div>
                <!-- end div#content -->
                <div id="sidebar">
                    <!--ul-->
                        <?php if ($_SESSION['isAdmin'] ){
                                include 'include/adminnav.php';
                                }else{
                                  include 'include/usernav.php';
                                } ?>
                        <!-- end navigation -->
                            <?php include 'include/updates.php'; ?>
                        <!-- end updates -->
                    <!--/ul-->
                </div>
                <!-- end div#sidebar -->
                <div style="clear: both; height: 1px"></div>
            </div>
                <?php include 'include/footer.php'; ?>
        </div>
        <!-- end div#wrapper -->
    </body>
</html>
