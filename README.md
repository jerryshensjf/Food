# PHP开源公司订饭系统

### 项目介绍
PHP开源公司订饭系统正式版发布 
Food Online System v1.0.0 发布 

Food Online System是开源公司订饭系统 
用于PHP,mysql学习。以及工作组，公司向第三方电话订餐的实际使用。 
所有程序在GPLv3条款下开源，关于GPLv3相关pdf已放置在doc子目录下。 
doc下另有所有版本的release not
  
请用如下管理员登录 
160208
jerry

数据库脚本在sql子目录下

Bug报告jerry_shen_sjf@qq.com

软件架构
软件架构说明
PHP, Mysql, MVC

### 版本更新
 
最近在Linux下使用xampp 7.4在PHP7上调通了开源公司订饭系统。

数据库是MariaDB 10。


### 项目截图

![Image description](https://images.gitee.com/uploads/images/2021/0916/172447_caac58ac_1203742.png "food_login.png")

![Image description](https://images.gitee.com/uploads/images/2021/0916/172501_82b2c33e_1203742.png "food_register.png")

![Image description](https://images.gitee.com/uploads/images/2021/0919/225202_871396e9_1203742.png "food_inner.png")